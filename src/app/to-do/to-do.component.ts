import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-to-do',
  templateUrl: './to-do.component.html',
  styleUrls: ['./to-do.component.scss']
})
export class ToDoComponent implements OnInit {

  private url = 'http://localhost:8080/todos';
  private content: string;
  todo$: Object;
  constructor(private data: DataService, private http: HttpClient) { }

  ngOnInit() {
    this.http.get(this.url).subscribe(todos => this.todo$ = todos);
  }

  onSubmit(event) {
    event.preventDefault();
    this.http.post(this.url, {
      content: this.content,
      date: new Date(),
    }).subscribe(todos => this.todo$ = todos);
    console.log(this.todo$);
  }

  onDelete(id) {
    const url = this.url + '/' + id;
    this.http.delete(url).subscribe(todos => this.todo$ = todos);
  }

}
