
export interface TODOJSON {
  todoId: string;
  content: string;
  date: string;
}

export class Todo {
  public todoId: string;
  public content: string;
  public date: string;


  constructor(todo: TODOJSON) {
      this.todoId = todo.todoId;
      this.content = todo.content;
      this.date = todo.date;
  }

}
