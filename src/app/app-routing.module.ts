import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LsidebarComponent } from './lsidebar/lsidebar.component';
import { ToDoComponent } from 'src/app/to-do/to-do.component';
import { HelloWorldComponent } from './hello-world/hello-world.component';
const routes: Routes = [
  {
    path: '',
    component: ToDoComponent
  },
  {
    path: 'helloworld',
    component: HelloWorldComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
