import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-hello-world',
  templateUrl: './hello-world.component.html',
  styleUrls: ['./hello-world.component.scss']
})
export class HelloWorldComponent {

  private name: string;
  constructor(private http: HttpClient) {}

  onSubmit(event) {
    event.preventDefault();
    const url = 'http://localhost:8080/hello/' + this.name ;
    this.http.get(url, {responseType: 'text'}).subscribe(greeting => console.log(greeting));
  }

}
